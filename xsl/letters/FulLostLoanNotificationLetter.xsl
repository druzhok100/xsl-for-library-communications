<?xml version="1.0" encoding="utf-8"?>

<!--

This generates the notification letter that a patron is about to be invoiced.

Created for JIRA Issue AA-7

-->

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>

  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>

      <body>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>

        <xsl:call-template name="head"/>
        <!-- header.xsl -->
        <xsl:call-template name="senderReceiver"/>
        <!-- SenderReceiver.xsl -->

        <br/>

        <table cellspacing="0" cellpadding="5" border="0">
          <xsl:attribute name="style">
            <xsl:call-template name="mainTableStyleCss"/>
            <!-- style.xsl -->
          </xsl:attribute>
          <tr>
            <td>
              We are writing to alert you that the following item is now 4 weeks overdue.
              If it is not returned within 2 weeks you will be invoiced for its replacement.
              Please see the following link for full details of
              <a href="http://www.lancaster.ac.uk/library/using-the-library/fines-and-charges/lost-books-and-replacement-charges">
                replacement charges</a>.
              <br/>
              <br/>
              Remember to check Your Library Account - Weekly Statement for full details of all your loans.
            </td>
          </tr>
        </table>

        <table cellpadding="5" class="listing" border="0">
          <xsl:attribute name="style">
            <xsl:call-template name="mainTableStyleCss"/>
            <!-- style.xsl -->
          </xsl:attribute>

          <xsl:for-each select="notification_data">
            <tr>
              <td>
                <b>Lost Item:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/title"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Author:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/author"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Loan date:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/loan_date"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Due date:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/due_date"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Barcode:</b>
              </td>
              <td>
                <xsl:value-of select="item_loan/barcode"/>
              </td>
            </tr>
            <tr>
              <td>
                <b>Classmark:</b>
              </td>
              <td>
                <xsl:value-of select="phys_item_display/call_number"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <br/>

        <xsl:call-template name="lastFooter"/>
        <!-- footer.xsl -->
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>