<?xml version="1.0" encoding="utf-8"?>
<!--
Generates email to patron when an item has become available on the hold shelf.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="mailReason.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>
  <xsl:include href="recordTitle.xsl"/>
  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>
      <body>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>
        <xsl:call-template name="head"/>
        <!-- header.xsl -->
        <xsl:call-template name="senderReceiver"/>
        <!-- SenderReceiver.xsl -->
        <xsl:call-template name="toWhomIsConcerned"/>
        <!-- Check for IDS letter -->
        <xsl:variable name="ids"><xsl:call-template name="is_ids"/></xsl:variable>
        <!-- mailReason.xsl -->
        <div class="messageArea">
          <div class="messageBody">
            <table cellspacing="0" cellpadding="5" border="0">
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <!-- If not IDS -->
              <xsl:if test="$ids != 'y'">
                <tr>
                  <td>@@following_item_requested_on@@ 
                    <xsl:value-of select="notification_data/request/create_date"/>, 
                    <xsl:if test="notification_data/user_for_printing/user_group!='12' 
                      and notification_data/user_for_printing/user_group!='30' ">
                      @@can_picked_at@@.
                      <p>
                        If you no longer need it, please cancel 
                        the request by signing into My Account on 
                        <a href="http://onesearch.lancs.ac.uk">Onesearch</a>.
                      </p>
                    </xsl:if>
                    <!-- If a distance learner -->
                    <xsl:if test="notification_data/user_for_printing/user_group='12'
                      or notification_data/user_for_printing/user_group='30'
                      or notification_data/user_for_printing/user_group='distance_staff' ">
                      will be posted to you.
                    </xsl:if>
                  </td>
                </tr>
                <!-- If not a distance learner -->
                <xsl:if test="notification_data/user_for_printing/user_group!='12' 
                  and notification_data/user_for_printing/user_group!='30' ">
                  <xsl:if test="notification_data/request/work_flow_entity/expiration_date">
                    <tr>
                      <td>
                        @@note_item_held_until@@ 
                        <xsl:value-of select="notification_data/request/work_flow_entity/expiration_date"/>.
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:if>
              </xsl:if>
              <!-- If IDS -->
              <xsl:if test="$ids = 'y'">
                  <tr>
                      <td>
                          <xsl:choose>
                            <xsl:when test="/notification_data/phys_item_display/material_type = 'Journal'">
                                A copy of the following article, which you requested on <xsl:value-of select="/notification_data/request/create_date"/>, has arrived for you to keep.
                                Please collect it from the information point during our
                                <a href="http://www.lancaster.ac.uk/library/using-the-library/opening-hours">staffed service hours</a>.
                            </xsl:when>
                            <xsl:otherwise>
                                Your interlending book has arrived. Please collect it from the information point
                                during our
                                <a href="http://www.lancaster.ac.uk/library/using-the-library/opening-hours">staffed service
                                hours</a>. Some books can only be used in the library - please check the title line below.<br/><br/>
                                Please collect and return your book promptly as the library can incur extra charges for renewals.<br/><br/>
                              </xsl:otherwise>
                             </xsl:choose>
                      </td>
                  </tr>
              </xsl:if>
              <tr>
                <td>
                  <xsl:call-template name="recordTitle"/>
                  <!-- recordTitle.xsl -->
                </td>
              </tr>
              <!-- <xsl:if test="notification_data/request/system_notes">
                <tr>
                  <td><b>@@notes_affect_loan@@:</b></td>
                </tr>
                <tr>
                  <td><xsl:value-of select="notification_data/request/system_notes"/></td>
                </tr>
              </xsl:if> -->
            </table>
            <br/>
            <table>
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <!-- If IDS -->
              <xsl:if test="$ids = 'y'">
                <tr>
                  <td>Yours sincerely,</td>
                </tr>
                <tr>
                  <td>Interlending and Document Supply</td>
                </tr>
                <tr>
                  <td>email: library.ids@lancaster.ac.uk</td>
                </tr>
                <tr>
                  <td>tel: (01524)5-92516</td>
                </tr>
              </xsl:if>
              <!-- otherwise -->
              <xsl:if test="$ids != 'y'">
                <tr>
                  <td>Yours sincerely,</td>
                </tr>
                <tr>
                  <td>Library User Services</td>
                </tr>
                <tr>
                  <td>email: library@lancaster.ac.uk</td>
                </tr>
                <tr>
                  <td>tel: (01524)5-92516</td>
                </tr>
              </xsl:if>
            </table>
          </div>
        </div>
        <xsl:call-template name="hr"/>
        <!-- footer.xsl -->
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
