<?xml version="1.0" encoding="utf-8"?>
<!--
Email patron a return receipt for items, of limited utility in our 
SC environment as this won't work when items are returned by SC. Only
works when items are returned by an Alma operator.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="mailReason.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>
  <xsl:include href="recordTitle.xsl"/>
  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>
      <body>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>
        <xsl:call-template name="head"/>
        <!-- header.xsl -->
        <xsl:call-template name="senderReceiver"/>
        <!-- SenderReceiver.xsl -->
        <br/>
        <xsl:call-template name="toWhomIsConcerned"/>
        <!-- mailReason.xsl -->
        <div class="messageArea">
          <div class="messageBody">
            <table cellspacing="0" cellpadding="5" border="0">
              <xsl:attribute name="style">
                <xsl:call-template name="mainTableStyleCss"/>
              </xsl:attribute>
              <tr>
                <td>
                  <p>The following items were returned to the library.</p>
                </td>
              </tr>
              <tr>
                <td>
                  <b>@@loans@@</b>
                </td>
              </tr>
              <tr>
                <td>
                  <table cellpadding="5" class="listing">
                    <xsl:attribute name="style">
                      <xsl:call-template name="mainTableStyleCss"/>
                      <!-- style.xsl -->
                    </xsl:attribute>
                    <tr>
                      <th>@@title@@</th>
                      <th>@@author@@</th>
                      <th>@@due_date@@</th>
                      <th>@@return_date@@</th>
                      <th>@@fine@@</th>
                    </tr>
                    <xsl:for-each select="notification_data/items/item_loan">
                      <tr>
                        <td>
                          <xsl:value-of select="title"/>
                        </td>
                        <td>
                          <xsl:value-of select="author"/>
                        </td>
                        <td>
                          <xsl:value-of select="new_due_date_str"/>
                        </td>
                        <td>
                          <xsl:value-of select="return_date_str"/>
                        </td>
                        <td>
                          <xsl:value-of select="fine"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
              <br/>
              <br/>
            </table>
            <br/>
          </div>
        </div>
        <!-- footer.xsl -->
        <xsl:call-template name="lastFooter"/>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
