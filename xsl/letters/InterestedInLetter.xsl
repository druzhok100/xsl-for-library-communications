<?xml version="1.0" encoding="utf-8"?>
<!--
Generates email when item is received and a patron has expressed an interest.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:include href="header.xsl"/>
  <xsl:include href="senderReceiver.xsl"/>
  <xsl:include href="mailReason.xsl"/>
  <xsl:include href="footer.xsl"/>
  <xsl:include href="style.xsl"/>
  <xsl:template match="/">
    <html>
      <head>
        <xsl:call-template name="generalStyle"/>
      </head>
      <body>
        <xsl:attribute name="style">
          <xsl:call-template name="bodyStyleCss"/>
          <!-- style.xsl -->
        </xsl:attribute>
        <xsl:call-template name="head"/>
        <!-- header.xsl -->
        <xsl:call-template name="senderReceiver"/>
        <!-- SenderReceiver.xsl -->
        <br/>
        <xsl:call-template name="toWhomIsConcerned"/>
        <!-- mailReason.xsl -->
        You have expressed an interest in the following order:
        <br/>
        <table cellspacing="0" cellpadding="5" border="0">
          <xsl:attribute name="style">
            <xsl:call-template name="mainTableStyleCss"/>
            <!-- style.xsl -->
          </xsl:attribute>
          <tr>
            <td>
              <br/>
              Order:
              <br/>
            </td>
            <td>
              <br/>
              <xsl:value-of select="notification_data/line_number"/>
              <br/>
            </td>
          </tr>
          <tr>
            <td>
              <br/>
              Title :
              <br/>
            </td>
            <td>
              <br/>
              <xsl:value-of select="notification_data/title"/>
              <br/>
            </td>
          </tr>
          <tr>
            <td>
              <br/>
              Note :
              <br/>
            </td>
            <td>
              <br/>
              <xsl:if test="notification_data/message='Item was received.'">
                This item is now being processed and will soon
                be available via <a href="http://onesearch.lancs.ac.uk">OneSearch</a>.
              </xsl:if>
              <xsl:if test="notification_data/message='E-resource was activated.'">
                This e-resource will be available within the next 24 hours via
                <a href="http://onesearch.lancs.ac.uk">OneSearch</a>.
              </xsl:if>
              <xsl:if test="notification_data/message!='Item was received.'
                and notification_data/message!='E-resource was activated.'">
                <xsl:value-of select="notification_data/message"/>
              </xsl:if>
              <br/>
            </td>
          </tr>
        </table>
        <br/>
        <xsl:call-template name="lastFooter"/>
        <!-- footer.xsl -->
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
