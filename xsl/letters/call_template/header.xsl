<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template name="head">
    <xsl:param name="logo" select="true()"/>
    <table>
      <xsl:attribute name="style">
        <xsl:call-template name="headerTableStyleCss"/>
      </xsl:attribute>
      <col/>
      <col width="123"/>
      <tr>
        <td>
          <a href="http://lancaster.ac.uk/library" style="color: #000000; font-size: 24pt; text-decoration: none;">
            Library
          </a>
        </td>
        <td style="width: 123px;">
          <a href="http://www.lancs.ac.uk" title="Lancaster University Homepage" 
            style="color: #000000; font-size: 24pt; text-decoration: none;">
            <!-- New branding required here -->
            <img src="http://www.lancaster.ac.uk/media/lancaster-university/style-assets/images/bg-logo.png"
              style="border: 0; margin: 0; padding: 0;" width="274" height="87" alt="Lancaster University Logo"/>
          </a>
        </td>
      </tr>
    </table>
    <xsl:call-template name="hr"/>
  </xsl:template>
  
  <xsl:template name="date-diff">
    <xsl:param name="date1"/>
    <xsl:param name="date2"/>
    <xsl:variable name="jdate1">
      <xsl:call-template name="date-julian">
        <xsl:with-param name="date" select="$date1"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="jdate2">
      <xsl:call-template name="date-julian">
        <xsl:with-param name="date" select="$date2"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$jdate1 - $jdate2"/>
  </xsl:template>
  <xsl:template name="date-julian">
    <!--
      Calcuate Julian date from YYYYMMDD[HHMMSS] (time optional)
        using formula from "http://aa.usno.navy.mil/faq/docs/JD_Formula.php"
    -->
    <xsl:param name="date"/>
    <xsl:variable name="year" select="number(substring($date,1,4))"/>
    <xsl:variable name="month" select="number(substring($date,5,2))"/>
    <xsl:variable name="day" select="number(substring($date,7,2))"/>
    <xsl:variable name="time">
      <xsl:choose>
        <xsl:when test="string-length($date) &gt; 8">
          <xsl:variable name="hour" select="number(substring($date,9,2))"/>
          <xsl:variable name="min" select="number(substring($date,11,2))"/>
          <xsl:variable name="sec" select="number(substring($date,13,2))"/>
          <xsl:value-of select="($sec + ($min * 60) + ($hour * 3600)) div 3600"/>
        </xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="adjust" select="(100 * $year) + $month - 190002.5"/>
    <xsl:variable name="sign">
      <xsl:choose>
        <xsl:when test="number($adjust) &lt; 0">-1</xsl:when>
        <xsl:otherwise>1</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="(367 * $year) - floor(7 * ($year + floor(($month + 9) div 12)) div 4) + floor((275 * $month) div 9) + $day + 1721013.5 + (number($time) div 24) - (0.5 * number($sign)) + 0.5"/>
  </xsl:template>

  <xsl:template name="lu-repeat-string">
    <xsl:param name="string" select="''"/>
    <xsl:param name="repeat" select="1"/>
    <xsl:choose>
      <xsl:when test="$repeat &lt; 1"></xsl:when>
      <xsl:when test="$repeat = 1">
        <xsl:value-of select="$string"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="s">
          <xsl:call-template name="lu-repeat-string">
            <xsl:with-param name="string" select="$string"/>
            <xsl:with-param name="repeat" select="$repeat - 1"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="concat( $string, $s )"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="lu-extend-string">
    <xsl:param name="direction" select="'right'"/>
    <xsl:param name="end"/>
    <xsl:param name="min-len" select="0"/>
    <xsl:param name="len"/>
    <xsl:param name="start"/>
    <xsl:variable name="str-len" select="$end - $start + 1"/>
    <xsl:variable name="diff" select="$str-len - $min-len"/>
    <xsl:choose>
      <xsl:when test="$direction = 'left'">
        <xsl:choose>
          <xsl:when test="$min-len &lt; 1">
            <xsl:value-of select="$start"/>
          </xsl:when>
          <xsl:when test="$diff &lt; 0">
            <xsl:choose>
              <xsl:when test="$start + $diff &lt; 1">
                <xsl:text>1</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$start + $diff"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$start"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$direction = 'right'">
        <xsl:choose>
          <xsl:when test="$min-len &lt; 1">
            <xsl:value-of select="$end"/>
          </xsl:when>
          <xsl:when test="$diff &lt; 0">
            <xsl:choose>
              <xsl:when test="$end - $diff &gt; $len">
                <xsl:value-of select="$len"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$end - $diff"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$end"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>0</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="lu-mask-string">
    <xsl:param name="for" select="''"/>
    <xsl:param name="from" select="1"/>
    <xsl:param name="mask" select="'*'"/>
    <xsl:param name="min-mask" select="0"/>
    <xsl:param name="string" select="''"/>
    <xsl:param name="to" select="''"/>
    <xsl:variable name="len" select="string-length($string)"/>
    <xsl:variable name="start">
      <xsl:choose>
        <xsl:when test="$from &lt; 1">
          <xsl:value-of select="$len + $from"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="number($from)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="end">
      <xsl:choose>
        <xsl:when test="$to = ''">
          <xsl:choose>
            <xsl:when test="number($for) &gt; 0">
              <xsl:value-of select="$from + number($for)"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$len"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$to &lt; 1">
          <xsl:value-of select="$len + $to"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$to"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="end1">
      <xsl:call-template name="lu-extend-string">
        <xsl:with-param name="direction" select="'right'"/>
        <xsl:with-param name="end" select="$end"/>
        <xsl:with-param name="len" select="$len"/>
        <xsl:with-param name="min-len" select="$min-mask"/>
        <xsl:with-param name="start" select="$start"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="start1">
      <xsl:call-template name="lu-extend-string">
        <xsl:with-param name="direction" select="'left'"/>
        <xsl:with-param name="end" select="$end1"/>
        <xsl:with-param name="len" select="$len"/>
        <xsl:with-param name="min-len" select="$min-mask"/>
        <xsl:with-param name="start" select="$start"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="maskstr">
      <xsl:call-template name="lu-repeat-string">
        <xsl:with-param name="string" select="$mask"/>
        <xsl:with-param name="repeat" select="$end1 - $start1 + 1"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of
        select="concat(substring($string, 1, $start1 - 1), $maskstr, substring($string, $end1 + 1) )"/>
  </xsl:template>

  <xsl:template name="is_ids">
    <!-- Return y for IDS-related notices, empty for non-IDS notices -->
    <xsl:choose>
      <xsl:when test="/notification_data/organization_unit/code = 'RES_SHARE'">y</xsl:when>
      <xsl:when test="/notification_data/phys_item_display/location_code = 'OUT_RS_REQ'">y</xsl:when>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
