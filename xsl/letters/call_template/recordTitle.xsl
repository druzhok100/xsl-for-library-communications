<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template name="recordTitle">
    <div class="recordTitle">
      <span class="spacer_after_1em">
        Title: <b><xsl:value-of select="notification_data/phys_item_display/title"/></b>
      </span>
    </div>
    <xsl:if test="notification_data/phys_item_display/author != ''">
      <div class="">
        <span class="spacer_after_1em">
          <span class="recordAuthor">
            Author: <b><xsl:value-of select="notification_data/phys_item_display/author"/></b>
          </span>
        </span>
      </div>
    </xsl:if>
    <xsl:if test="notification_data/phys_item_display/barcode != ''">
      <div class="">
        <span class="spacer_after_1em">
          <span class="recordBarcode">
            Barcode: <b><xsl:value-of select="notification_data/phys_item_display/barcode"/></b>
          </span>
        </span>
      </div>
    </xsl:if>
    <xsl:if test="notification_data/phys_item_display/call_number != ''">
      <div class="">
        <span class="spacer_after_1em">
          <span class="recordClassmark">
            Classmark: <b><xsl:value-of select="notification_data/phys_item_display/call_number"/></b>
          </span>
        </span>
      </div>
    </xsl:if>
    <!-- 
    <xsl:if test="notification_data/phys_item_display/issue_level_description">
      <div class="">
        <span class="spacer_after_1em">
          <span class="volumeIssue">
            Description: <xsl:value-of select="notification_data/phys_item_display/issue_level_description"/>
          </span>
        </span>
      </div>
    </xsl:if> 
    -->
  </xsl:template>
</xsl:stylesheet>
