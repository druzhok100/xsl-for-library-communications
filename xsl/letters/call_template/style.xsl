<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:template name="generalStyle">
    <style>
      body {background-color:#fff}
      .listing td {border-bottom: 1px solid #eee}
      .listing tr:hover td {background-color:#eee}
      .listing th {background-color:#f5f5f5 }
      h4{line-height: 0.2em}
     </style>
  </xsl:template>

  <xsl:template name="ajh_lines">
    <xsl:param name="lines" select="."/>
    <xsl:if test="string(.) != ''">
      <xsl:value-of select="."/>
      <br/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="baseStyle">
    font-family: Calibri,Arial,Helvetica,sans-serif; font-size: 12pt; font-weight: normal;
    background-color: #ffffff; color: #000000;
  </xsl:template>

  <xsl:template name="blockStyle"><xsl:call-template name="baseStyle"/>
       margin-right: 42px;
  </xsl:template>

  <xsl:template name="hr">
    <hr style="color: #d52b1e; margin-right: 42px"/>
  </xsl:template>

  <xsl:template name="bodyStyleCss">
    <xsl:call-template name="baseStyle"/>
    margin:0; padding:0;
  </xsl:template>

  <xsl:template name="listStyleCss">
    <xsl:call-template name="baseStyle"/>
    list-style: none; margin:0 0 0 1em; padding:0
  </xsl:template>

  <xsl:template name="mainTableStyleCss">
    <xsl:call-template name="baseStyle"/>
    width:100%; text-align:left
  </xsl:template>

  <xsl:template name="headerLogoStyleCss">
    <xsl:call-template name="baseStyle"/>
    width:100%;
  </xsl:template>

  <xsl:template name="headerTableStyleCss">
    <xsl:call-template name="baseStyle"/>
    width:100%;
  </xsl:template>

  <xsl:template name="footerTableStyleCss">
    <xsl:call-template name="baseStyle"/>
    width:100%; margin-top:1em;  line-height:2em;
  </xsl:template>

</xsl:stylesheet>
